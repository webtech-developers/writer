@extends('setup.index')
@section('title', 'Service ')
@section('setting_page')

@include('setup.partials.action_toolbar', [
 'title' => (isset($urgency->id)) ? 'Edit urgency' : 'Create Order Source', 
 'hide_save_button' => TRUE,
 'back_link' => ['title' => 'back to source', 'url' => route("source_list")],
])
 <form role="form" class="form-horizontal" enctype="multipart/form-data" action="{{ (isset($source->id)) ? route( 'source_update', $source->id) : route('source_store') }}" method="post" autocomplete="off" >
   {{ csrf_field()  }}
   @if(isset($source->id))
   {{ method_field('PATCH') }}
   @endif
   <div class="form-group">
      <label>Name <span class="required">*</span></label>
      <input type="text" class="form-control form-control-sm {{ showErrorClass($errors, 'name') }}" name="name" value="{{ old_set('name', NULL, $source) }}">
      <div class="invalid-feedback">{{ showError($errors, 'name') }}</div>
   </div>
   
   <div class="form-group">
      <div class="custom-control custom-checkbox">
         <input type="checkbox" class="custom-control-input" id="inactive" name="inactive" value="1" {{ old_set('inactive', NULL, $source) ? 'checked="checked"' : '' }}>
         <label class="custom-control-label" for="inactive">Inactive</label>
      </div>
   </div>
   <input type="submit" name="submit" class="btn btn-success" value="Submit"/>
</form>
@endsection


@push('scripts')
<script>
    $(function(){           
      $('select').select2({
          theme: 'bootstrap4',
         minimumResultsForSearch: -1
      });     

    });      
</script>
@endpush
