<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class WorkLevel extends Model
{

    protected $fillable = [
        'id',
        'name',
        
        'inactive'
    ];

    protected $casts = [
        'inactive' => 'boolean'
    ];
}
