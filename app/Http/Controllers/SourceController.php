<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Yajra\DataTables\Facades\DataTables;
use Illuminate\Support\Facades\Validator;
use App\Source;
use Illuminate\Validation\Rule;

class SourceController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('setup.source.index');
    }

    public function datatable(Request $request)
    {
        $source = Source::orderBy('name', 'DESC');

        if (! $request->include_inactive_items) {
            $source->whereNull('inactive');
        }

        return Datatables::eloquent($source)->editColumn('type', function ($source) {
            return '<a href="' . route('source_edit', $source->id) . '">' . $source->name .   '</a>';
        })
          
            ->addColumn('status', function ($source) {
            return ($source->inactive) ? 'Inactive' : 'Active';
        })
            ->addColumn('action', function ($source) {

            return '<a class="btn btn-sm btn-danger delete-item" href="' . route('source_delete', $source->id) . '"><i class="fas fa-minus-circle"></i></a>';
        })
            ->rawColumns([
            'name',
            
            'action'
        ])
            ->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $source = new \StdClass();

       

        return view('setup.source.create', compact('source', ));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|',
            
            
        ], [

            'name.unique' => 'The source name already exists'
        ]);

       

        $request['inactive'] = (isset($request->inactive)) ? TRUE : NULL;

        Source::create($request->all());

        return redirect()->back()->withSuccess('Successfully created a new source');
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Source $source)
    {
        $data = Source::dropdown();

        return view('setup.source.create', compact('source', 'data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|',
            
            
        ], [
            'name.unique' => 'The source already exists'
        ]);

        

        $request['inactive'] = (isset($request->inactive)) ? TRUE : NULL;

        Source::where('id', $id)->update($request->only('name',   'inactive'));

        return redirect()->back()->withSuccess('Successfully updated the source item');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, Source $source)
    {
        $redirect = redirect()->route('source_list');

        try {

            $source->delete();
            $redirect->withSuccess('Successfully deleted');
        } catch (\Illuminate\Database\QueryException $e) {

            $redirect->withFail('You cannot delete the source as it is associated with one or multiple orders');
        } catch (\Exception $e) {
            $redirect->withFail('Could not perform the requested action');
        }

        return $redirect;
    }
}
