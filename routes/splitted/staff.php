<?php

Route::prefix('browse-work')->group(function () {  

	Route::get('/', 'TaskController@browse_work')
	->name('browse_work');

	Route::post('/', 'TaskController@datatable_browse_work')
	->name('browse_work_datatable');

	Route::post('details/{order}', 'TaskController@self_assign_task')
	->name('accept_work');

});


